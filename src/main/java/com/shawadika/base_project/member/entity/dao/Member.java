package com.shawadika.base_project.member.entity.dao;

import lombok.*;


import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Member {

    @Id
    @GeneratedValue
    private Long id;

    private String nickName;

    /**
     * nullable:是否允许为null,默认为true
     * unique:是否唯一,默认为false
     */
    @Column(unique = true , nullable = false , length = 20)
    private String account;

    @Column(nullable = false)
    private String password;

    //这里如果不设置fetch=FetchType.EAGER,查询member时候会报错,
    //参考;https://blog.csdn.net/xu180/article/details/53396750
    @ManyToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
    private Set<Role> role;

}
