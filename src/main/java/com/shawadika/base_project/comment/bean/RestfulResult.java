package com.shawadika.base_project.comment.bean;

import lombok.*;

/**
 * restful API
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RestfulResult {

    public static final int SUCCESS=200;

    public static final int FAIL=0;

    private Integer code;

    private String msg;

    private Object object;

    public static RestfulResult success(Object object){

        return RestfulResult.builder()
                .code(SUCCESS)
                .msg("success")
                .object(object)
                .build();
    }

    public static RestfulResult success(String msg,Object object){

        return RestfulResult.builder()
                .code(SUCCESS)
                .msg(msg)
                .object(object)
                .build();
    }


    public static RestfulResult success(){

        return RestfulResult.builder()
                .code(SUCCESS)
                .msg("success")
                .build();
    }

    public static RestfulResult fail(String msg,Object obj){
        return RestfulResult.builder()
                .code(FAIL)
                .msg(msg)
                .object(obj)
                .build();
    }

    public static RestfulResult fail(String msg){
        return RestfulResult.builder()
                .code(FAIL)
                .msg(msg)
                .build();
    }
}
