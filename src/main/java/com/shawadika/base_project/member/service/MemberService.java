package com.shawadika.base_project.member.service;

import com.shawadika.base_project.member.entity.dao.Member;


public interface MemberService {

    Member register(Member member);

    Member findMemberByAccount(String account);
}
