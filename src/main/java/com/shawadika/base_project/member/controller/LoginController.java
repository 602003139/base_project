package com.shawadika.base_project.member.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping("/login.html")
    public ModelAndView loginPage (ModelAndView view){
        view.setViewName("login/login");
        return view;
    }

    @GetMapping("/reg.html")
    public ModelAndView register (ModelAndView view){
        view.setViewName("login/reg");
        return view;
    }
}
