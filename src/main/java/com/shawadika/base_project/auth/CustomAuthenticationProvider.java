package com.shawadika.base_project.auth;

import com.shawadika.base_project.member.entity.dao.Member;
import com.shawadika.base_project.member.service.MemberService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * 自定义身份认证验证组件
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private final MemberService memberService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public CustomAuthenticationProvider(MemberService memberService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.memberService = memberService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //登录验证密码逻辑
        String account = authentication.getName();
        String password = authentication.getCredentials().toString();
        //验证账号密码
        Member member = memberService.findMemberByAccount(account);
        if(member != null){
            //校验密码
            boolean checkResult = bCryptPasswordEncoder.matches(password,member.getPassword());
            if(!checkResult){
                throw new BadCredentialsException("密码错误~");
            }
            // 这里设置权限和角色
            ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(){
                {
                    member.getRole().forEach(item->add(new GrantedAuthorityImpl(item.getRole())));
                }
            };
            // 生成令牌
            return new UsernamePasswordAuthenticationToken(account, password, authorities);
        }else{
            throw new BadCredentialsException("账号错误~");
        }
    }

    // 是否可以提供输入类型的认证服务
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
