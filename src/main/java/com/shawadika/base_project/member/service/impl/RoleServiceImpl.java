package com.shawadika.base_project.member.service.impl;

import com.shawadika.base_project.member.entity.dao.Member;
import com.shawadika.base_project.member.entity.dao.Role;
import com.shawadika.base_project.member.resp.MemberResp;
import com.shawadika.base_project.member.resp.RoleResp;
import com.shawadika.base_project.member.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Set;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleResp roleResp;

    private final MemberResp memberResp;

    @Autowired
    public RoleServiceImpl(RoleResp roleResp, MemberResp memberResp) {
        this.roleResp = roleResp;
        this.memberResp = memberResp;
    }

    @Override
    public Member configMemberRole(long memberId,long roleId) {
        Role role = roleResp.getOne(roleId);
        Member member = memberResp.getOne(memberId);
        member.getRole().addAll(Collections.singletonList(role));
        return memberResp.save(member);
    }

    @Override
    public Set<Member> findMembersByRole(String roleName) {
        Role roleByRole = roleResp.findRoleByRole(roleName);
        return roleByRole.getMember();
    }
}


















