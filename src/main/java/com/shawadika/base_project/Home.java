package com.shawadika.base_project;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class Home {

    @RequestMapping("")
    public ModelAndView home(ModelAndView modelAndView){
        modelAndView.setViewName("index");
        return modelAndView;
    }

    @GetMapping("/index.html")
    public ModelAndView index (ModelAndView view){
        view.setViewName("index/home");
        return view;
    }
}
