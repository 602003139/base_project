package com.shawadika.base_project.member.resp;

import com.shawadika.base_project.member.entity.dao.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleResp extends JpaRepository<Role,Long> {

    Role findRoleByRole(String roleName);
}
