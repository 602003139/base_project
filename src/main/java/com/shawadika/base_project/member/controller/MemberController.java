package com.shawadika.base_project.member.controller;

import com.shawadika.base_project.comment.BaseController;
import com.shawadika.base_project.comment.bean.RestfulResult;
import com.shawadika.base_project.member.entity.dao.Member;
import com.shawadika.base_project.member.entity.dao.Role;
import com.shawadika.base_project.member.entity.dto.MemberDTO;
import com.shawadika.base_project.member.service.MemberService;
import com.shawadika.base_project.member.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/member")
public class MemberController extends BaseController {

    private final MemberService memberService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    private final RoleService roleService;

    @Autowired
    public MemberController(MemberService memberService, BCryptPasswordEncoder bCryptPasswordEncoder, RoleService roleService) {
        this.memberService = memberService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.roleService = roleService;
    }

    @RequestMapping(value="/register",method=RequestMethod.POST)
    @ResponseBody
    public RestfulResult register (@Valid MemberDTO memberDTO, BindingResult bindingResult){
        if(!bindingResult.hasErrors()){
            //加密密码
            String encodePassword = bCryptPasswordEncoder.encode(memberDTO.getPassword());
            memberDTO.setPassword(encodePassword);
            Member member = memberService.register(memberDTO.convertToMember());
            System.out.println(member);
            return RestfulResult.success(memberDTO.convertFor(member));
        }
        return RestfulResult.fail("参数检验失败",formatErrMsg(bindingResult));
    }

    @RequestMapping(value="/config_role",method=RequestMethod.POST)
    @ResponseBody
    public RestfulResult addRoleToMember (Long memberId,Long roleId){

        if (memberId!=null && roleId !=null){
            roleService.configMemberRole(memberId, roleId);
            return RestfulResult.success();
        }
        return RestfulResult.fail("参数检验失败");
    }

    @RequestMapping(value="/get_by_role",method=RequestMethod.POST)
    @ResponseBody
    public RestfulResult getMembersByRole (String roleName){
        Set<Member> members = roleService.findMembersByRole(roleName);
        members.forEach(System.out::println);
        return RestfulResult.success(members);
    }
}













