package com.shawadika.base_project.comment;

import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseController {

    protected List<String> formatErrMsg(@NotNull BindingResult bindingResult){
         return new ArrayList<String>(){
            {
                for (ObjectError error : bindingResult.getAllErrors()) {
                    add(error.getDefaultMessage());
                }
            }
        };
    }
}
