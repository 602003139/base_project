package com.shawadika.base_project.member.entity;

import lombok.Data;

@Data
public class User {

    private String account;
    private String password;
}
