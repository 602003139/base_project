package com.shawadika.base_project.member.entity.dto;

import com.google.common.base.Converter;
import com.shawadika.base_project.member.entity.dao.Member;
import com.shawadika.base_project.member.entity.dao.Role;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.Set;


@Data
@ToString
public class MemberDTO {


    @Length(min = 5 , max = 20 ,message = "账号长度在5到20位之间")
    private String account;

    @Length(min = 6 , max = 20 ,message = "密码长度在6到20位之间")
    private String password;

    @Length(max = 10 ,message = "昵称长度不得超过10个字符")
    private String nickName;

    private Set<Role> role = new HashSet<>();

    public Member convertToMember(){
        if(StringUtils.isEmpty(this.getNickName())){
            this.setNickName("大灰灰");
        }
        return new MemberDTOConvert().doForward(this);
    }

    public MemberDTO convertFor(Member member){
        return new MemberDTOConvert().doBackward(member);
    }

    private static class MemberDTOConvert  extends Converter<MemberDTO,Member> {

        @Override
        protected Member doForward(MemberDTO memberDTO) {
            Member member = new Member();
            BeanUtils.copyProperties(memberDTO,member);
            return member;
        }

        @Override
        protected MemberDTO doBackward(Member member) {
            MemberDTO memberDTO = new MemberDTO();
            BeanUtils.copyProperties(member,memberDTO);
            return memberDTO;
        }
    }
}
