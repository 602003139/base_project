package com.shawadika.base_project.member.service.impl;

import com.shawadika.base_project.member.entity.dao.Member;
import com.shawadika.base_project.member.entity.dao.Role;
import com.shawadika.base_project.member.resp.MemberResp;
import com.shawadika.base_project.member.role.RoleConfig;
import com.shawadika.base_project.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MemberServiceImpl implements MemberService {

    private final MemberResp memberResp;

    @Autowired
    public MemberServiceImpl(MemberResp memberResp) {
        this.memberResp = memberResp;
    }

    @Override
    public Member register(Member member) {
        //默认普通用户
        Role normalRole = Role.builder().role(RoleConfig.normal).build();
        member.getRole().addAll(Collections.singletonList(normalRole));
        return memberResp.save(member);
    }

    @Override
    public Member findMemberByAccount(String account) {
        return memberResp.findMemberByAccount(account);
    }
}
