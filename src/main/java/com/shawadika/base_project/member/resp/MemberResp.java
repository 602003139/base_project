package com.shawadika.base_project.member.resp;

import com.shawadika.base_project.member.entity.dao.Member;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MemberResp extends JpaRepository<Member,Long> {

    Member findMemberByAccount(String account);

}
