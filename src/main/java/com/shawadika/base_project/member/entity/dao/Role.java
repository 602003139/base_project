package com.shawadika.base_project.member.entity.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Role {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique=true)
    private String role;

    @ManyToMany(mappedBy="role",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JsonIgnore
    private Set<Member> member;
}
