package com.shawadika.base_project.member.service;


import com.shawadika.base_project.member.entity.dao.Member;

import java.util.Set;

public interface RoleService {

     Member configMemberRole(long memberId, long roleId);

     Set<Member> findMembersByRole(String roleName);
}
